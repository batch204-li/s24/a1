// console.log("Hello, 204!");

let num = 2;

const getCube = num ** 3;
console.log(`The cube of ${num} is ${getCube}`);


const address = ["258 Washington Ave NW", "California", "90011"];

const [streetAddress, stateAddress, postalCode] = address;

console.log(`I live at ${address}`);

const animal = {
	givenName: "Lolong",
	weight: "1075 kgs",
	length: "20 ft 3 in."
};

function animalInfo({givenName, weight, length}) {
	console.log(`${givenName} was a saltwater crocodile. He weighed at ${weight} with a measurement of ${length}`);
}

animalInfo(animal);


const numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => {
	console.log(number);
});



class Dog {
	constructor (name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog();

myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Miniature Dachshund";

console.log(myDog);

